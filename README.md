# s2m-infra

1. Currently docker-compose uses local images, so build all s2m necessary images locally
   1. build image for recording-processing
   2. build image for recording-capture
   3. build image for recording-preview   
2. `docker-compose up -d localstack`
3. `docker-compose up -d keycloak`
3. `docker-compose up -d recording-capture` (wait while localstack to be ready)
4. `docker-compose up -d recording-processing` (wait while localstack to be ready)
5. `docker-compose up -d recording-preview` (wait while localstack to be ready)
6. `docker-compose up -d redis`