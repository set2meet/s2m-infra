echo "*************************INIT*S3****************************************"
awslocal s3 mb s3://s2m-app-recording

echo "*************************INIT*SQS***************************************"
awslocal sqs create-queue --queue-name s2m_recording_task.fifo --attributes "DelaySeconds=5,VisibilityTimeout=300,ContentBasedDeduplication=true"
awslocal sqs create-queue --queue-name s2m_recording_complete.fifo --attributes "DelaySeconds=5,VisibilityTimeout=300,ContentBasedDeduplication=true"

echo "USE ONE PORT FOR ALL SERVICES!!! - PORT 4566"
echo "*************************USEFUL*COMMANDS********************************"
echo "aws --endpoint-url http://localhost:4566 sqs list-queues"
echo "aws --endpoint-url http://localhost:4566 sqs receive-message --queue-url QueueUrl"
echo "aws --endpoint-url http://localhost:4566 s3 ls"
echo "aws --endpoint-url http://localhost:4566 s3 ls s2m-app-recording/dev-dump/"
